using UnityEngine;
using System.Collections;

public class FootStepController : MonoBehaviour {
	
	//AudioClip
	public AudioClip[] concrete;
	public AudioClip[] wood;
	public AudioClip[] dirt;
	public AudioClip[] metal;
	
	//Info
	private bool step = true;
	private bool grounded;
	private bool crouching;
	private bool walking;
	private Transform obj;
	public float audioStepLengthWalk = 0.35f;
	public float audioStepLengthLowWalk = 0.65f;
	private RaycastHit hit;
	
	//Script
	CharacterMotor characterMotor;
	CrouchScript crouchScript;
	
	
	void Awake(){
		characterMotor = gameObject.GetComponent<CharacterMotor>();
		crouchScript = gameObject.GetComponent<CrouchScript>();
		grounded = characterMotor.grounded;
		crouching = crouchScript.crouching;
		audio.volume = .7f;
	}
	
	void Update(){
		
		// -- Step informations
		grounded = characterMotor.grounded;
		crouching = crouchScript.crouching;
		if(Input.GetAxis("Horizontal")!=0f||Input.GetAxis("Vertical")!=0f){
			walking = true;
		}else{
			walking = false;
		}
		Vector3 direction = transform.TransformDirection(Vector3.down);
		if(Physics.Raycast(transform.position, direction, out hit , 10)){
			obj = hit.transform;
		}
		// -- Up to here
		
		if(grounded && obj.gameObject.tag == "Untagged" && walking && step){
			if(crouching){
				StartCoroutine(LowWalkOnConcrete(audioStepLengthLowWalk));
			}else{
				StartCoroutine(WalkOnConcrete(audioStepLengthWalk));
			}
		}
		
		if(grounded && obj.gameObject.tag == "Concrete" && walking && step){
			if(crouching){
				StartCoroutine(LowWalkOnConcrete(audioStepLengthLowWalk));
			}else{
				StartCoroutine(WalkOnConcrete(audioStepLengthWalk));
			}
		}
		
		if(grounded && obj.gameObject.tag == "Metal" && walking && step){
			if(crouching){
				StartCoroutine(LowWalkOnMetal(audioStepLengthLowWalk));
			}else{
				StartCoroutine(WalkOnMetal(audioStepLengthWalk));
			}
		}
		
		if(grounded && obj.gameObject.tag == "Dirt" && walking && step){
			if(crouching){
				StartCoroutine(LowWalkOnDirt(audioStepLengthLowWalk));
			}else{
				StartCoroutine(WalkOnDirt(audioStepLengthWalk));
			}
		}
		
		if(grounded && obj.gameObject.tag == "Wood" && walking && step){
			if(crouching){
				StartCoroutine(LowWalkOnWood(audioStepLengthLowWalk));
			}else{
				StartCoroutine(WalkOnWood(audioStepLengthWalk));
			}
		}

	}
	
	
	
	private IEnumerator LowWalkOnConcrete(float audioStepLenghLowWalk){
		step = false;
		audio.clip = concrete[Random.Range(0, concrete.Length)];
		//audio.Play();
		yield return new WaitForSeconds (audioStepLengthLowWalk);
		step = true;
	}
	private IEnumerator WalkOnConcrete(float audioStepLengthWalk){
		step = false;
		audio.clip = concrete[Random.Range(0, concrete.Length)];
		audio.Play();
		yield return new WaitForSeconds (audioStepLengthWalk);
		step = true;
	}
	
	private IEnumerator LowWalkOnMetal(float audioStepLenghLowWalk){
		step = false;
		audio.clip = metal[Random.Range(0, metal.Length)];
		//audio.Play();
		yield return new WaitForSeconds (audioStepLengthLowWalk);
		step = true;
	}
	private IEnumerator WalkOnMetal(float audioStepLengthWalk){
		step = false;
		audio.clip = metal[Random.Range(0, metal.Length)];
		audio.Play();
		yield return new WaitForSeconds (audioStepLengthWalk);
		step = true;
	}
	
	private IEnumerator LowWalkOnDirt(float audioStepLenghLowWalk){
		step = false;
		audio.clip = dirt[Random.Range(0, dirt.Length)];
		//audio.Play();
		yield return new WaitForSeconds (audioStepLengthLowWalk);
		step = true;
	}
	private IEnumerator WalkOnDirt(float audioStepLengthWalk){
		step = false;
		audio.clip = dirt[Random.Range(0, dirt.Length)];
		audio.Play();
		yield return new WaitForSeconds (audioStepLengthWalk);
		step = true;
	}
	
	private IEnumerator LowWalkOnWood(float audioStepLenghLowWalk){
		step = false;
		audio.clip = wood[Random.Range(0, wood.Length)];
		//audio.Play();
		yield return new WaitForSeconds (audioStepLengthLowWalk);
		step = true;
	}
	private IEnumerator WalkOnWood(float audioStepLengthWalk){
		step = false;
		audio.clip = wood[Random.Range(0, wood.Length)];
		audio.Play();
		yield return new WaitForSeconds (audioStepLengthWalk);
		step = true;
	}
	
}

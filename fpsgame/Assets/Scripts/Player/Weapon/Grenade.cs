using UnityEngine;
using System.Collections;

public class Grenade : MonoBehaviour {
	
	public bool networkMode;
	
	// GameObjects for Script
	public GameObject weaponAnim;
	public GameObject mainCameraRoot;
	public GameObject weaponManager;
	public GameObject mainCamera;
	public GameObject weaponCamera;
	public GameObject player;
	public GameObject rootGO;
	
	public Transform defAimPoint;
	public Transform target;
	
	//For instantiate objects
	public GameObject grenade;
	
	//Crosshair Textures
	public Texture2D crosshairHorizontal;
	public Texture2D crosshairVertical;
	private float adjustMaxCroshairSize = 6.0f;
	
	public bool drawing = false;
	public bool loading = false;
	public bool shooting = false;
	// - time
	public float loadTime = 1.2f;
	public float delay = 0.2f;
	// - bullets
	public int maxBulletsLeft = 2;
	public int bulletsLeft = 2;
	
	//---- Audio
	public GameObject shotSound;
	public AudioClip loadSound;
	public AudioClip drawSound;
	
	// Refer scripts
	CharacterMotor characterMotor;
	RecoilController recoilController;
	MovementAnim movementAnim;
	
	
	// --- Include scripts in Awake
	void Awake(){
		if(bulletsLeft<=0){
			foreach(Transform child in gameObject.transform){
				child.gameObject.SetActiveRecursively(false);
			}
		}
		characterMotor = player.GetComponent<CharacterMotor>();
		recoilController = rootGO.GetComponent<RecoilController>();
		movementAnim = player.GetComponent<MovementAnim>();
	}
	
	
	public IEnumerator DrawWeapon(){
		if(bulletsLeft!=0){
		// -- Format down to here --
		mainCameraRoot.animation.Play("CameraIdle");
		mainCameraRoot.animation.Play("CameraDraw");
		animation.Play("Reset");
		drawing = true;
		audio.clip = drawSound;
		audio.Play();
		loading = false;
		shooting = false;
		movementAnim.aiming = false;
		// -- Format up to here
		weaponAnim.animation.Stop();
		weaponAnim.animation.Play("Draw", PlayMode.StopAll);
		//weaponAnim.animation.Play("Draw");
		yield return new WaitForSeconds(1.0f);
		drawing = false;
		}
	}
	
	
	void Update(){
		if(Input.GetButtonDown("Fire1")&&!drawing&&!shooting&&!loading&&bulletsLeft!=0){
			StartCoroutine(Fire(delay));
		}
		if(bulletsLeft<=0){
			foreach(Transform child in gameObject.transform){
				child.gameObject.SetActiveRecursively(false);
			}
		}
	}
	
	private IEnumerator Fire(float delay){
		shooting = true;
		animation.Stop();
		animation.Play("Shot");
		Instantiate(shotSound,transform.position,Quaternion.identity);
		yield return new WaitForSeconds(delay);
		bulletsLeft--;
		Vector3 direction = new Vector3(target.transform.position.x-defAimPoint.position.x,target.transform.position.y-defAimPoint.position.y,target.transform.position.z-defAimPoint.position.z);
		GameObject grenade1 = Instantiate(grenade,defAimPoint.position,Quaternion.identity) as GameObject;
		grenade1.rigidbody.AddForce(direction*0.000073f);
		shooting = false;
		loading = true;
		animation.Play("Reset");
		StartCoroutine(Load(loadTime));
	}
	
	private IEnumerator Load(float time){
		weaponAnim.animation.Play("Draw");
		yield return new WaitForSeconds(time);
		loading = false;
	}
	
	void OnGUI(){
		float triggerTime = 0.2f;
		//Crosshair
		float w = crosshairHorizontal.width;
		float h = crosshairHorizontal.height;
		Rect position1 = new Rect((Screen.width + w)/2 + (triggerTime * adjustMaxCroshairSize),(Screen.height - h)/2, w, h);
		Rect position2 = new Rect((Screen.width - w)/2,(Screen.height + h)/2 + (triggerTime * adjustMaxCroshairSize), w, h);
		Rect position3 = new Rect((Screen.width - w)/2 - (triggerTime * adjustMaxCroshairSize) - w,(Screen.height - h )/2, w, h);
		Rect position4 = new Rect((Screen.width - w)/2,(Screen.height - h)/2 - (triggerTime * adjustMaxCroshairSize) - h, w, h);
		GUI.DrawTexture(position1, crosshairHorizontal); 	//Right
		GUI.DrawTexture(position2, crosshairVertical); 	//Up
		GUI.DrawTexture(position3, crosshairHorizontal); 	//Left
		GUI.DrawTexture(position4, crosshairVertical);		//Down
	}
	
}

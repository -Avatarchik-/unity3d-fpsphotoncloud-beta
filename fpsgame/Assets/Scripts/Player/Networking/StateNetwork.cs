using UnityEngine;
using System.Collections;

public class StateNetwork : Photon.MonoBehaviour {
	
	public int health = 100;
	public int weaponMode;
	public GameObject nowWeapon;
	public bool crouching;
	
	public GameObject weaponManagerObject;
	WeaponManager weaponManager;
	
	public GameObject player;
	CrouchScript crouchScript;
	
	
	void Awake(){
		weaponManager = weaponManagerObject.GetComponent<WeaponManager>();
		crouchScript = player.GetComponent<CrouchScript>();
	}
	
	void Update(){
		if(photonView.isMine){
			
			// What a weapon player has
			weaponMode = weaponManager.weaponMode;
			switch (weaponMode){
			case 0 : break;
			case 1 : nowWeapon = weaponManager.mainWeapon;
					 break;
			case 2 : nowWeapon = weaponManager.secondWeapon;
					 break;
			case 3 : nowWeapon = weaponManager.subWeapon;
					 break;
			}
			
			crouching = crouchScript.crouching;
			
		}
	}
	
}

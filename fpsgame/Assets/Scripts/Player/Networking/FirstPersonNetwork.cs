using UnityEngine;
using System.Collections;

public class FirstPersonNetwork : Photon.MonoBehaviour {
	
	public GameObject mainCamera;
	public GameObject weaponCamera;
	
	CharacterMotor controllerScript;
	MouseLook cameraScript;
	MouseLook cameraScriptMain;
	
	CrouchScript crouchScript;
	MovementAnim movementAnimScript;
	FootStepController footStepController;
    

    void Awake()
    {
        controllerScript = GetComponent<CharacterMotor>();
		cameraScriptMain = mainCamera.GetComponent<MouseLook>();
		cameraScript = GetComponent<MouseLook>();
		crouchScript = GetComponent<CrouchScript>();
		movementAnimScript = GetComponent<MovementAnim>();
		footStepController = GetComponent<FootStepController>();

         if (photonView.isMine)
        {
           	mainCamera.camera.enabled = true;
            cameraScriptMain.enabled = true;
			cameraScript.enabled = true;
            controllerScript.enabled = true;
			
			crouchScript.enabled = true;
			movementAnimScript.enabled = true;
			footStepController.enabled = true;
        }
        else
        {   
			mainCamera.camera.enabled = false;
            cameraScriptMain.enabled = false;
			cameraScript.enabled = false;
            controllerScript.enabled = false;
			
			crouchScript.enabled = false;
			movementAnimScript.enabled = false;
			footStepController.enabled = false;
        }

        gameObject.name = gameObject.name + photonView.viewID.ID;
    }

    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            stream.SendNext(transform.position);
            stream.SendNext(transform.rotation); 
        }
        else
        {
            correctPlayerPos = (Vector3)stream.ReceiveNext();
            correctPlayerRot = (Quaternion)stream.ReceiveNext();
        }
    }

    private Vector3 correctPlayerPos = Vector3.zero; //We lerp towards this
    private Quaternion correctPlayerRot = Quaternion.identity; //We lerp towards this

    void Update()
    {
        if (!photonView.isMine)
        {
            //Update remote player (smooth this, this looks good, at the cost of some accuracy)
            transform.position = Vector3.Lerp(transform.position, correctPlayerPos, Time.deltaTime * 5);
            transform.rotation = Quaternion.Lerp(transform.rotation, correctPlayerRot, Time.deltaTime * 5);
        }
    }

}

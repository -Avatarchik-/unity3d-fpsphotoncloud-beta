using UnityEngine;
using System.Collections;

public class WalkScript : MonoBehaviour {
	
	public bool walking = false;
	CharacterMotor characterScript;
	
	void Awake(){
		characterScript = gameObject.GetComponent<CharacterMotor>();
	}
	
	void Update () {
		if(Input.GetButton("Walk")){
			walking = true;
		}else{
			walking = false;
		}
		
		if(walking){
			characterScript.movement.maxForwardSpeed = 3.0f;
			characterScript.movement.maxBackwardsSpeed = 3.0f;
			characterScript.movement.maxSidewaysSpeed = 3.0f;
		}else{
			characterScript.movement.maxForwardSpeed = 7.0f;
			characterScript.movement.maxBackwardsSpeed = 7.0f;
			characterScript.movement.maxSidewaysSpeed = 7.0f;
		}
		
	}
}

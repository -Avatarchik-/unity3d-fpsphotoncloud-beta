using UnityEngine;
using System.Collections;

public class CrouchScript : MonoBehaviour {
	
	public GameObject rootGO;
	public GameObject crouchTransform;
	public GameObject normalTransform;
	public GameObject characterColliderObject;
	
	public float maxSpeed;
	public float crouchSpeed;
	
	public bool crouching = false;
	public bool isCrouchMovingDown = false;
	public bool isCrouchMovingUp;
	public float smooth = 10.0f;
	
	private Transform maxHeight;
	private Transform minHeight;
	private RaycastHit hit;
	
	CharacterMotor characterScript;
	CharacterController controller;
	CharacterColliderHit colliderScript;
	
	void Awake(){
		maxHeight = normalTransform.transform;
		minHeight = crouchTransform.transform;
		characterScript = gameObject.GetComponent<CharacterMotor>();
		controller = gameObject.GetComponent<CharacterController>();
		colliderScript = characterColliderObject.GetComponent<CharacterColliderHit>();
	}
	
	void Update(){
		Vector3 direction = transform.TransformDirection(Vector3.up);
		
		if(Input.GetButton("Crouch")){
			crouching = true;
		}else if(!Input.GetButton("Crouch")&&!Physics.Raycast(transform.position, direction, out hit , 1)&&!colliderScript.onColStay){
			crouching = false;
		}
		
		if(crouching){
			rootGO.transform.localPosition = Vector3.Lerp(rootGO.transform.localPosition, minHeight.localPosition, Time.deltaTime*smooth);
			characterScript.movement.maxForwardSpeed = crouchSpeed;
			characterScript.movement.maxSidewaysSpeed = crouchSpeed;
			characterScript.movement.maxBackwardsSpeed = crouchSpeed;
			controller.center = new Vector3(0,-0.5f,0);
			controller.height = 1.0f;
		}else{
			rootGO.transform.localPosition = Vector3.Lerp(rootGO.transform.localPosition, maxHeight.localPosition, Time.deltaTime*smooth*1.5f);
			characterScript.movement.maxForwardSpeed = maxSpeed;
			characterScript.movement.maxSidewaysSpeed = maxSpeed;
			characterScript.movement.maxBackwardsSpeed = maxSpeed;
			controller.center = new Vector3(0,0,0);
			controller.height = 2.0f;
		}
		
	}
	
}

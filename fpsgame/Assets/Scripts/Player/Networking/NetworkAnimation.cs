using UnityEngine;
using System.Collections;

public class NetworkAnimation : Photon.MonoBehaviour {
	
	public GameObject[] weapons;
	public GameObject player;
	public GameObject spine;
	public GameObject camera;
	public GameObject soldier;
	
	public Transform up;
	public Transform rightUp;
	public Transform leftUp;
	public Transform right;
	public Transform left;
	
	public bool crouching;
	
	private GameObject selectingWeapon;
	private Quaternion correctSpineRot = Quaternion.identity;
	private float horiz;
	private float vert;
	
	StateNetwork stateNetwork;
	
	void Awake(){
		stateNetwork = player.GetComponent<StateNetwork>();
		if(photonView.isMine){
			foreach (Transform child in gameObject.transform){
				child.gameObject.SetActiveRecursively(false);
			}
		}
	}
	
	void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info){
		if(stream.isWriting){
			stream.SendNext(camera.transform.rotation); 
			stream.SendNext(Input.GetAxis("Horizontal"));
			stream.SendNext(Input.GetAxis("Vertical"));
			stream.SendNext(Input.GetButton("Crouch"));
		}else{
			correctSpineRot = (Quaternion)stream.ReceiveNext();
			horiz = (float)stream.ReceiveNext();
			vert = (float)stream.ReceiveNext();
			crouching = (bool)stream.ReceiveNext();
		}
	}
	
	void Update(){
		if(!photonView.isMine){
			spine.transform.rotation = Quaternion.Lerp(spine.transform.rotation, correctSpineRot, Time.deltaTime * 5);
			if(vert>0&&horiz>0){
				soldier.transform.rotation = Quaternion.Lerp(soldier.transform.rotation, rightUp.rotation, Time.deltaTime * 5);
			}else if(vert>0&&horiz<0){
				soldier.transform.rotation = Quaternion.Lerp(soldier.transform.rotation, leftUp.rotation, Time.deltaTime * 5);
			}else if(vert<0&&horiz>0){
				soldier.transform.rotation = Quaternion.Lerp(soldier.transform.rotation, leftUp.rotation, Time.deltaTime * 5);
			}else if(vert<0&&horiz<0){
				soldier.transform.rotation = Quaternion.Lerp(soldier.transform.rotation, rightUp.rotation, Time.deltaTime * 5);
			}else if(horiz>0&&vert==0){
				soldier.transform.rotation = Quaternion.Lerp(soldier.transform.rotation, right.rotation, Time.deltaTime * 5);
			}else if(horiz<0&&vert==0){
				soldier.transform.rotation = Quaternion.Lerp(soldier.transform.rotation, left.rotation, Time.deltaTime * 5);
			}else if(vert>0&&horiz==0){
				soldier.transform.rotation = Quaternion.Lerp(soldier.transform.rotation, up.rotation, Time.deltaTime * 5);
			}else if(vert<0&&horiz==0){
				soldier.transform.rotation = Quaternion.Lerp(soldier.transform.rotation, up.rotation, Time.deltaTime * 5);
			}else if(vert==0&&horiz==0){
				soldier.transform.rotation = Quaternion.Lerp(soldier.transform.rotation, up.rotation, Time.deltaTime * 5);
			}
			if(horiz!=0||vert!=0){
				if(!crouching){
					animation.CrossFade("WalkForward",0.1f);
				}else{
					animation.CrossFade("CrouchForward",0.1f);
				}
			}else{
				if(!crouching){
					animation.CrossFade("StandIdle",0.1f);
				}else{
					animation.CrossFade("CrouchIdle",0.1f);
				}
			}
		}
	}
	
}

using UnityEngine;
using System.Collections;

public class TitleRoll : MonoBehaviour {
	
	public GameObject plate;
	
	public Texture2D fadeTexture;
	private bool isFade = false;
	private float cnt;
	
	public Texture2D[] textures;
	
	public float speed;
	public float waitTime;
	private float waitingTime = 0;
	private int nowMode = 0;
	private bool isRotating = false;
	
	void Awake(){
		StartCoroutine(Roll(waitTime));
		fadeTexture = new Texture2D(1,1);
	}
	
	private IEnumerator Roll(float time){
		int i;
		yield return new WaitForSeconds(time);
		for(i=0;i<=textures.Length-1;i++){
			plate.renderer.material.mainTexture = textures[i];
			yield return new WaitForSeconds(time);
		}
		isFade = true;
		yield return new WaitForSeconds(time);
		Application.LoadLevel("Menu");
	}
	
	void OnGUI(){
		if(isFade){
			fadeTexture.SetPixel(0, 0, new Color(0, 0, 0, cnt));
			fadeTexture.Apply();
			GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), fadeTexture);
			cnt += Time.deltaTime * speed;
		}
	}
	
	
	
}

using UnityEngine;
using System.Collections;

public class MuzzleFlash : MonoBehaviour {
	
	public float muzzleFlashLength;
	public float maxScale;
	public bool mcnt;
	public bool activating;
	public GameObject flashLight;
	public Light realflashLight;
	float rnd;
	
	void Awake(){
		flashLight.renderer.enabled=false;
		mcnt = false;
		activating = false;
	}
	
	void Update(){
		if(mcnt&&!activating){
			StartCoroutine(MuzzleFlashOn(muzzleFlashLength));
		}
	}
	
	public IEnumerator MuzzleFlashOn(float time){
		activating=true;
		transform.localEulerAngles = new Vector3(0,Random.Range(0.0f,360.0f),0);
		rnd=Random.Range(maxScale/2.0f, maxScale);
    	transform.localScale = new Vector3(rnd,rnd,rnd);
    	flashLight.renderer.enabled = true;
		realflashLight.enabled=true;
    	yield return new WaitForSeconds(time);
    	flashLight.renderer.enabled = false;
		realflashLight.enabled=false;
		mcnt=false;
		activating=false;
	}
	
}

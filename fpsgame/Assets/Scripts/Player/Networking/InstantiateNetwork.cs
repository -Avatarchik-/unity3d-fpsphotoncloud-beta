using UnityEngine;
using System.Collections;

public class InstantiateNetwork : Photon.MonoBehaviour {
	
	
	//Bullet hole
	// - metal
	public GameObject bulletHoleMetal;
	public GameObject spark;
	// - default / concreto
	public GameObject bulletHole;
	public GameObject bulletSmoke;
	
	
	[RPC]
	void MetalHole(Vector3 pos, Vector3 hitnormal, Quaternion vnorm){
		print("instantiate metalhole");
		GameObject metal1 = Instantiate(bulletHoleMetal, pos, vnorm) as GameObject;
		GameObject metal2 = Instantiate(spark, pos, vnorm) as GameObject;
		//metal1.transform.parent = hittrans;
		//metal2.transform.parent = hittrans;
	}
	
	[RPC]
	void DefaultHole(Vector3 pos, Vector3 hitnormal, Quaternion vnorm){
		print("instantiate defaulthole");
		GameObject default1 = Instantiate(bulletHole, pos, vnorm) as GameObject;
		GameObject default2 = Instantiate(bulletSmoke, pos,  Quaternion.LookRotation(hitnormal)) as GameObject;
		//default1.transform.parent = hittrans;
		//default2.transform.parent = hittrans;
	}
	
}

using UnityEngine;
using System.Collections;

public class CharacterColliderHit : MonoBehaviour {
	
	public bool onColStay;
	
	void OnCollisionStay(Collision collision) {
		onColStay = true;
	}
	
}

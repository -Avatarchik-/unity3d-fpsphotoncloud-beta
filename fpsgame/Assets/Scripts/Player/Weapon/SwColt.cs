using UnityEngine;
using System.Collections;

public class SwColt : MonoBehaviour {
	
	//GameObjects for Script
	public GameObject weaponAnim;
	public GameObject camera;
	public GameObject muzzleFlash;
	public GameObject bulletHole;
	public GameObject muzzleSmoke;
	public GameObject player;
	public GameObject rootGO;
	public GameObject weaponManager;
	
	// Status ------------------------------
	public bool drawing = false;
	public bool loading = false;
	public bool shooting = false;
	public bool outTimeIng = false;
	// - time
	public float shotSpeed = 0.1f;
	public float loadTime = 2.0f;
	public float outTime = 0.5f;
	// - bullets
	public int maxBulletsLeft = 13;
	public int bulletsLeft = 13;
	// -------------------------------------
	
	//Audio
	public GameObject shotSound;
	public AudioClip loadSound;
	public AudioClip drawSound;
	public AudioClip outOfAmmo;
	
	//Refer scripts
	CharacterMotor characterMotor;
	RecoilController recoilController;
	
	
	
	
	//Include scripts in Awake
	void Awake(){
		characterMotor = player.GetComponent<CharacterMotor>();
		recoilController = rootGO.GetComponent<RecoilController>();
	}
	
	
	
	public IEnumerator DrawWeapon(){
			
			drawing = true;
			weaponAnim.animation.Stop();
			weaponAnim.animation.Play("Draw");
			audio.clip = drawSound;
			audio.Play();
			loading = false;
			shooting = false;
			outTimeIng = false;
			muzzleFlash.GetComponent<MuzzleFlash>().mcnt = false;
			muzzleFlash.GetComponent<MuzzleFlash>().realflashLight.enabled = false;
			muzzleFlash.GetComponent<MuzzleFlash>().flashLight.renderer.enabled =false;
			muzzleFlash.GetComponent<MuzzleFlash>().activating = false;
			yield return new WaitForSeconds(1.0f);
			drawing = false;
			
	}
	
	
	
	void Update(){

		if(Input.GetButtonDown("Fire1")&&!drawing&&!shooting&&!loading&&bulletsLeft>=1){
			StartCoroutine(Shot(shotSpeed));
		}
		if(Input.GetButton("Fire1")&&bulletsLeft<=0&&!loading&&!drawing&&!shooting){
			StartCoroutine(Load(loadTime));
		}
		if(Input.GetButtonDown("Reload")&&!drawing&&!loading){
			if(bulletsLeft != maxBulletsLeft){
				StartCoroutine(Load(loadTime));
			}
		}
		if(!Input.GetButton("Fire1")&&bulletsLeft<=0&&!loading&&!drawing){
			StartCoroutine(Load(loadTime));
		}	
			
	}
	
	
	
	private IEnumerator Shot(float time){
		shooting = true;
		bulletsLeft--;
		weaponAnim.animation.Stop();
		weaponAnim.animation.Play("Shot");
		Instantiate(shotSound,transform.position,Quaternion.identity);
		yield return new WaitForSeconds(time);
		shooting = false;
	}
	
	private IEnumerator Load(float time){
		loading = true;
		weaponAnim.animation.Play("Reload");
		audio.clip = loadSound;
		audio.Play();
		yield return new WaitForSeconds(time);
		bulletsLeft = maxBulletsLeft;
		loading = false;
	}
	
	
	
}

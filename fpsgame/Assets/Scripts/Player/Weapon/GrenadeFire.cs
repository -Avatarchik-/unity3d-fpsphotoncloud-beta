using UnityEngine;
using System.Collections;

public class GrenadeFire : MonoBehaviour {
	
	public GameObject explosion;
	public float delay;
	
	void Awake(){
		StartCoroutine(Fire(delay));
	}
	
	
	private IEnumerator Fire(float time){
		yield return new WaitForSeconds(time);
		Instantiate(explosion,transform.localPosition,Quaternion.identity);
		Destroy(gameObject);
	}
	
}

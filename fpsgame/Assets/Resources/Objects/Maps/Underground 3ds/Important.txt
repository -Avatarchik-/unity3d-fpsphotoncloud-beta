Underground Scene copyright Tony Hayes 2007 all rights reserved.
www.my-art-gallery.co.uk

There are three versions of this file available from my website and from ShareCG.

1. VOB version for Vue 6inf (Latest update) This version may need to be resized to fit characters.
2  Poser 7 version (pz3)
3  3ds version for other software and earlier versions of Poser and Vue. This version may need to be resized to fit characters.

All versions are fully textured and all of the textures were created by me.
You may use the content for your personal and commercial artwork but please do not redistribute the original files.

If you enjoy using this scene you may like to make a small donation towards the upkeep of my website, there is a PayPal donation button at the foot of the website page.


Why not check out the rest of my free stuff too!

Enjoy!
Tony Hayes
aka Bogwoppet


Whilst every care has been taken during the creation of this free item, I cannot be held responsible for any damage caused to your system, software or hardware.
using UnityEngine;
using System.Collections;

public class MovementAnim : MonoBehaviour {
	
	CharacterMotor characterMotor;
	public GameObject weaponManager;
	public GameObject cameraAnim;
	public bool aiming = false;

	void Awake () {
		characterMotor = gameObject.GetComponent<CharacterMotor>();
	}
	
	void Update () {
		if(characterMotor.grounded){
			if(Input.GetAxis("Horizontal")!=0f||Input.GetAxis("Vertical")!=0f){
				if(Input.GetButton("Crouch")){
					cameraAnim.animation.CrossFade("CameraWalk");
					if(aiming){
						weaponManager.animation.CrossFade("AimWalk");
					}else{
						weaponManager.animation.CrossFade("LowWalk");
					}
				}else{
					cameraAnim.animation.CrossFade("CameraWalk");
					if(aiming){
						weaponManager.animation.CrossFade("AimWalk");
					}else{
						cameraAnim.animation.CrossFade("CameraWalk");
						weaponManager.animation.CrossFade("Walk");
					}
				}
			}else{
				cameraAnim.animation.CrossFade("CameraIdle");
				if(aiming){
					weaponManager.animation.CrossFade("AimIdle");
				}else{
					weaponManager.animation.CrossFade("Idle");
				}
			}
		}else if(characterMotor.jumping.jumping){
			weaponManager.animation.CrossFade("Idle");
		}else{
			if(aiming){
				weaponManager.animation.CrossFade("AimWalk");
			}else{
				weaponManager.animation.CrossFade("Idle");
			}
		}
	}
}

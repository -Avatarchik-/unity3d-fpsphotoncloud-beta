using UnityEngine;
using System.Collections;

public class DelayScript : MonoBehaviour {

	public float amount = 0.02f;
	public float maxAmount = 0.03f;
	public float smooth = 3.0f;
	private Vector3 def;

	void Update(){

		float factorX = -Input.GetAxis("Mouse X") * amount;
		float factorY = -Input.GetAxis("Mouse Y") * amount;

		if(factorX > maxAmount) factorX = maxAmount;
		if(factorX < -maxAmount) factorX = -maxAmount;
		if(factorY > maxAmount) factorX = maxAmount;
		if(factorY < -maxAmount) factorX = -maxAmount;
	
		Vector3 final = new Vector3(def.x + factorX, def.y + factorY, def.z);
		transform.localPosition = Vector3.Lerp(transform.localPosition, final, Time.deltaTime * smooth);

	}
}

using UnityEngine;
using System.Collections;

public class SelectedButton : MonoBehaviour {
	
	public Texture2D defaultTexture;
	public Texture2D selectedTexture;
	
	private bool selected = false;
	private bool flag;
	
	void Update(){
		if(selected){
			gameObject.renderer.material.mainTexture = selectedTexture;
			if(Input.GetButtonDown("Fire1")){
				StartCoroutine(SetLevel(0f));
			}
		}else{
			gameObject.renderer.material.mainTexture = defaultTexture;
		}
		selected = false;
	}
	
	void Selected(){
		selected = true;
	}
	
	IEnumerator SetLevel(float time){
		yield return new WaitForSeconds(time);
		if(gameObject.name=="SinglePlay"){
			Screen.lockCursor = false;
		    Screen.showCursor = true;
			Application.LoadLevel("game");
		}
		if(gameObject.name=="OnlinePlay"){
			Screen.lockCursor = false;
		    Screen.showCursor = true;
			Application.LoadLevel("GameOnline");
		}
		if(gameObject.name=="Options") print ("sorry not provided");
		if(gameObject.name=="Exit"){
			Application.Quit();
		    Screen.lockCursor = false;
		    Screen.showCursor = true;
		}
	}
	
}

using UnityEngine;
using System.Collections;

public class ViewDestroyer : Photon.MonoBehaviour {
	
	public float time;
	public bool viewIsMineToDestroy = false;

	void Awake(){
		if(viewIsMineToDestroy){
			if(photonView.isMine){
				Destroy(gameObject,time);
			}
		}else{
			if(!photonView.isMine){
				Destroy(gameObject,time);
			}
		}
	}
}

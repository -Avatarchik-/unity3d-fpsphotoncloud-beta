using UnityEngine;
using System.Collections;

/*
 * This script controls the weapon change action.
 * Script have right to change enable the weapon's SetActiveRecursively
 * Base script by EdoFrank [http://www.edofrank.com/]
 */


public class WeaponManager : MonoBehaviour {
	
	public bool networkMode;
	
	// weapons info ---
	public GameObject[] weapons;
	public GameObject mainWeapon;
	public GameObject secondWeapon;
	public GameObject subWeapon;
	public int weaponMode = 1;
	
	// Camera info ---
	public GameObject mainCamera;
	private float defMainCamFV; //default
	public GameObject weaponCamera;
	private float defWeaponCamFV; //default
	
	public Transform aimRoot;
	public Transform noAimingPosition;
	
	
	void Awake(){
		
		defMainCamFV = mainCamera.camera.fieldOfView;
		defWeaponCamFV = weaponCamera.camera.fieldOfView;
		
		weaponMode = 0; //not selected

		killAll();
			/*
			foreach (Transform child in mainWeapon.transform){
				child.gameObject.SetActiveRecursively(true);
			}
			mainWeapon.SetActiveRecursively(true);
			mainWeapon.SendMessage("DrawWeapon",SendMessageOptions.DontRequireReceiver);
			weaponMode = 1;
			*/
	}
	
	
	public void setWeapons(GameObject first, GameObject second, GameObject third){
	
			mainWeapon = first; 
			secondWeapon = second;
			subWeapon = third;
			weaponMode = 1;
			
			foreach (Transform child in mainWeapon.transform){
				child.gameObject.SetActiveRecursively(true);
			}
			mainWeapon.SetActiveRecursively(true);
		
	}
	
	
	void Update(){
		
			if(Input.GetButtonDown("1") && weaponMode != 1){
				killAll(); 
				foreach (Transform child in mainWeapon.transform){
					child.gameObject.SetActiveRecursively(true);
				}
				mainWeapon.SetActiveRecursively(true);
				mainWeapon.SendMessage("DrawWeapon",SendMessageOptions.DontRequireReceiver);
				weaponMode = 1;
			}
		
			if(Input.GetButtonDown("2") && weaponMode != 2){
				killAll();
				foreach (Transform child in secondWeapon.transform){
					child.gameObject.SetActiveRecursively(true);
				}
				secondWeapon.SetActiveRecursively(true);
				secondWeapon.SendMessage("DrawWeapon",SendMessageOptions.DontRequireReceiver);
				weaponMode = 2;
			}
		
			if(Input.GetButtonDown("3") && weaponMode != 3){
				killAll();
				foreach (Transform child in subWeapon.transform){
					child.gameObject.SetActiveRecursively(true);
				}
				subWeapon.SetActiveRecursively(true);
				subWeapon.SendMessage("DrawWeapon",SendMessageOptions.DontRequireReceiver);
				weaponMode = 3;
			}
	}
	
	
	void killAll(){
		
		mainCamera.camera.fieldOfView = defMainCamFV;
		weaponCamera.camera.fieldOfView = defWeaponCamFV;
		
		aimRoot.localPosition = noAimingPosition.localPosition;
		
		int i;
		for(i=0; i<weapons.Length; i++){
			weapons[i].SetActiveRecursively(false);
			foreach(Transform child in weapons[i].transform){
				child.gameObject.SetActiveRecursively(false);
			}
		}
		
		/*
		foreach(Transform child in mainWeapon.transform){
			child.gameObject.SetActiveRecursively(false);
		}
		mainWeapon.SetActiveRecursively(false);
		foreach(Transform child in secondWeapon.transform){
			child.gameObject.SetActiveRecursively(false);
		}
		secondWeapon.SetActiveRecursively(false);
		foreach(Transform child in subWeapon.transform){
			child.gameObject.SetActiveRecursively(false);
		}
		subWeapon.SetActiveRecursively(false);
		*/
	}
	
}

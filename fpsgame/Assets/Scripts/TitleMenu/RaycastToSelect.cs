using UnityEngine;
using System.Collections;

public class RaycastToSelect : MonoBehaviour {
	
	RaycastHit hit;
	
	void Awake(){
		Screen.lockCursor = true;
	    Screen.showCursor = false;
	}
	
	void Update(){
		Vector3 fwd = gameObject.transform.TransformDirection(Vector3.forward); 
		if(Physics.Raycast(gameObject.transform.position, fwd, out hit , 100)){
			hit.transform.gameObject.SendMessage("Selected",SendMessageOptions.DontRequireReceiver);
		}
	}
	
}
